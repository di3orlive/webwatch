var allDigits = [
    "img/0.png",
    "img/1.png",
    "img/2.png",
    "img/3.png",
    "img/4.png",
    "img/5.png",
    "img/6.png",
    "img/7.png",
    "img/8.png",
    "img/9.png"
];

var leftTop = $('.clock-hours-left .digit');
var rightTop = $('.clock-hours-right .digit');
var leftBot = $('.clock-minutes-left .digit');
var rightBot = $('.clock-minutes-right .digit');
var digit = $('.digit');

setInterval(function(){
    var date = new Date;
    var hour = ''+date.getHours();
    var minutes = ''+date.getMinutes();


    if(hour.length === 1){
        $(leftTop).css({
            "background" : "url('" + allDigits[0] + "') no-repeat center"
        });
        $(leftTop).attr("data-number", 0);


        $(rightTop).css({
            "background" : "url('" + allDigits[hour] + "') no-repeat center"
        });
        $(rightTop).attr("data-number", hour);
    }
    else
    {
        $(leftTop).css({
            "background" : "url('" + allDigits[hour.charAt(0)] + "') no-repeat center"
        });
        $(leftTop).attr("data-number", hour.charAt(0));


        $(rightTop).css({
            "background" : "url('" + allDigits[hour.charAt(1)] + "') no-repeat center"
        });
        $(rightTop).attr("data-number", hour.charAt(1));
    }

    if(minutes.length === 1){
        $(leftBot).css({
            "background" : "url('" + allDigits[0] + "') no-repeat center"
        });
        $(leftBot).attr("data-number", 0);


        $(rightBot).css({
            "background" : "url('" + allDigits[minutes] + "') no-repeat center"
        });
        $(rightBot).attr("data-number", minutes);
    }
    else
    {
        $(leftBot).css({
            "background" : "url('" + allDigits[minutes.charAt(0)] + "') no-repeat center"
        });
        $(leftBot).attr("data-number", minutes.charAt(0));


        $(rightBot).css({
            "background" : "url('" + allDigits[minutes.charAt(1)] + "') no-repeat center"
        });
        $(rightBot).attr("data-number", minutes.charAt(1));
    }


    $('[data-number="0"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg0');
    $('[data-number="1"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg1');
    $('[data-number="2"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg2');
    $('[data-number="3"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg3');
    $('[data-number="4"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg4');
    $('[data-number="5"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg5');
    $('[data-number="6"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg6');
    $('[data-number="7"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg7');
    $('[data-number="8"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg8');
    $('[data-number="9"]').parent().removeClass('bg0 bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9').addClass('bg9');

},1000);

